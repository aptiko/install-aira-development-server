#!/bin/bash
#
# This script is intended to install an aira development server on a
# Debian/Ubuntu server.

set -e
distribution_codename=`lsb_release --codename|awk '{ print $2 }'`

# Setup some distribution-specific variables
error=0
case "$distribution_codename" in
	trusty)		 localegen="/var/lib/locales/supported.d/local";;
	jessie | xenial) localegen="/etc/locale.gen";;
	*)		 error=1;;
esac
case "$distribution_codename" in
	trusty) postgresql_postgis="postgresql-9.3-postgis-2.1";;
	jessie) postgresql_postgis="postgresql-9.4-postgis-2.1";;
	xenial) postgresql_postgis="postgresql-9.5-postgis-2.2";;
	*)	error=1;;
esac
if [ $error = "1" ]; then
	echo "This operating system is not supported" >&2
	exit 1
fi

# Activate UTF-8 
if ! grep -q '^en_US.UTF-8 UTF-8' $localegen; then
	sudo echo 'en_US.UTF-8 UTF-8' >>$localegen
	sudo locale-gen
fi
export LC_ALL=en_US.UTF-8

# Install necessary packages
sudo apt-get install -y build-essential virtualenvwrapper python-celery \
	python-numpy python-gdal python-pip python-markupsafe python-psycopg2 \
	postgresql postgis $postgresql_postgis git rsync python-pil \
        python-celery

cd /
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh

# Create database user
if ! sudo -u postgres psql -c '\dg'|grep -q aira; then
	sudo -u postgres createuser aira --createdb
	sudo -u postgres psql -c "ALTER USER aira PASSWORD 'topsecret';"
fi

# Create database template
if ! sudo -u postgres psql -c '\l'|grep -q template_postgis; then
	sudo -u postgres createdb template_postgis
	sudo -u postgres psql -d template_postgis -c "CREATE EXTENSION postgis;"
	sudo -u postgres psql -d template_postgis -c \
		"UPDATE pg_database SET datistemplate='true' \
		WHERE datname='template_postgis';"
fi

# Create database
if ! sudo -u postgres psql -c '\l'|grep -q aira; then
	sudo -u postgres createdb aira --owner=aira --template=template_postgis
fi

# Install dickinson
if ! [ -e /usr/local/lib/libdickinson.so ]; then
	cd /tmp
	rm -rf 0.2.1.tar.gz dickinson-0.2.1
	wget https://github.com/openmeteo/dickinson/archive/0.2.1.tar.gz
	tar xzf 0.2.1.tar.gz
	cd dickinson-0.2.1
	./configure
	make
	sudo make install
	sudo ldconfig
fi

# Continue in user's directory
cd
mkdir -p bin cache coeffs_data

# Clone aira, aira-irma, pthelma
for x in aira aira-irma pthelma; do
	if ! [ -d $x ]; then
		git clone https://github.com/openmeteo/${x}.git
	fi
done

# Aira virtualenv
if ! [ -d ~/.virtualenvs/aira ]; then
	mkvirtualenv --system-site-packages aira || true
fi
source ~/.virtualenvs/aira/bin/activate

# Aira requirements
cd aira
pip install -r requirements.txt

# Aira settings
if ! [ -f aira_project/settings/local.py ]; then
	cat <<-EOF1 >aira_project/settings/local.py
		from .base import *

		DEBUG = True
		TEMPLATE_DEBUG = DEBUG
		EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
		SECRET_KEY = 'topsecret'
		TEMPLATE_DIRS = ['$HOME/aira-irma']
		STATICFILES_DIRS = ['$HOME/aira-irma/static']

		DATABASES = {
		    'default': {
		        'ENGINE': 'django.contrib.gis.db.backends.postgis',
		        'NAME': 'aira',
		        'USER': 'aira',
		        'PASSWORD': 'topsecret',
		        'HOST': 'localhost',
		        'PORT': 5432,
		        'CONN_MAX_AGE': 600,
		    }
		}

		CACHES = {
		    'default': {
		        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		        'LOCATION': '$HOME/cache/aira_cache',
		    },
		}

		AIRA_DATA_HISTORICAL = '$HOME/cache/rasters_historical'
		AIRA_DATA_FORECAST = '$HOME/cache/rasters_forecast'
	EOF1
fi

# Make sure we use the pthelma directory and not the virtualenv's pthelma
if ! grep -q PYTHONPATH ~/.bashrc; then
	echo 'export PYTHONPATH=~/pthelma' >>~/.bashrc
fi

# Migrate the database
cd ~/aira
./manage.py migrate

# Create the syncaira script
cat <<-EOF1 >~/bin/syncaira
	#!/bin/bash
	# Gets aira's production database and installs it locally (including the
	# rasters).

	set -e
	megdobas=megdobas.irrigation-management.eu

	# Get the database
	ssh \$megdobas "sudo -u postgres pg_dump aira" >/tmp/aira.dump
	sudo -u postgres dropdb aira
	sudo -u postgres createdb aira --owner=aira --template=template_postgis
	sudo -u postgres psql aira -c '\i /tmp/aira.dump'

	# Get the historical and forecast rasters
	cd ~/cache
	rsync -avzu --delete \$megdobas:/var/cache/pthelma/rasters_historical/ rasters_historical
	rsync -avzu --delete \$megdobas:/var/cache/pthelma/rasters_forecast/ rasters_forecast

	# Get the config rasters
	cd ~/coeffs_data
	rsync -avzu --delete \$megdobas:/etc/pthelma/*.tif .

	# Run swb
	cd ~/aira
	~/.virtualenvs/aira/bin/python manage.py runswb

	cd
	echo "Success!"
EOF1
chmod 755 ~/bin/syncaira

# Create the runaira script
cat <<-EOF1 >~/bin/runaira
	cd
	gnome-terminal --command='bash -c "echo -ne \"\033]0;Celery\007\" && cd aira && source ~/.virtualenvs/aira/bin/activate && celery -A aira worker -l info"'
	gnome-terminal --command='bash -c "echo -ne \"\033]0;Server\007\" && cd aira && source ~/.virtualenvs/aira/bin/activate && ./manage.py runserver 0.0.0.0:8000"'
EOF1
chmod 755 ~/bin/runaira

echo
echo Everything installed successfully
